#! /bin/sh
# Written for 22.04.4

sudo apt update
sudo apt autoremove -y dirmngr software-properties-common

# Install nala for faster and more beautiful apt
sudo apt install nala curl -y

# Set up nala
sudo nala fetch --auto
sudo nala update
sudo nala upgrade -y


# Install packages
sudo nala install -y gcc cmake make gettext perl curl wget htop btop git neofetch
# dev dependencies
sudo nala install -y gpg libssl-dev libbz2-dev libsqlite3-dev libreadline-dev liblzma-dev build-essential zlib1g-dev llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev python-tk python3-tk ca-certificates gnupg

# =============================
# =========== Python ==========
# =============================
# install pyenv
curl https://pyenv.run | bash
echo 'export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
 ' >> ~/.bashrc 

# install python versions
pyenv update
# pyenv install 3.9.15
pyenv install 3.11.9
pyenv global 3.11.9


# Neovim
wget https://github.com/neovim/neovim/archive/refs/tags/stable.tar.gz
tar xzvf stable.tar.gz
cd neovim-stable
make CMAKE_BUILD_TYPE=RelWithDebInfo
sudo make install
sudo cp ./build/bin/nvim /usr/bin



echo 'alias ll="ls -lAh " ' >> ~/.bashrc
echo 'alias sv="source .venv/bin/activate " ' >> ~/.bashrc
mkdir ~/.config
mkdir ~/projects
mkdir ~/software

# Install Brave Browser
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo nala update
sudo nala install brave-browser

# Install Flatpak, Flathub & Bitwarden Desktop
sudo nala install flatpak
sudo nala install gnome-software-plugin-flatpak
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
# reboot needed for full installation

# =============================
# ========= oh-my-tmux ========
# =============================
mkdir ~/software/oh-my-tmux
git clone https://github.com/gpakosz/.tmux.git ~/software/oh-my-tmux/
mkdir -p ~/.config/tmux
ln -s ~/software/oh-my-tmux/.tmux.conf ~/.config/tmux/tmux.conf
cp ~/software/oh-my-tmux/.tmux.conf.local ~/.config/tmux/tmux.conf.local




# =============================
# ======= Install Docker ======
# =============================
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo nala update
sudo nala install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo groupadd docker
sudo usermod -aG docker $USER
#Podman
sudo nala install -y podman
flatpak install flathub io.podman_desktop.PodmanDesktop -y

# =============================
# ============ IDEs ===========
# =============================
# install vscodium
# sudo wget https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg -O /usr/share/keyrings/vscodium-archive-keyring.asc
# echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.asc ] https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs vscodium main' \
#     | sudo tee /etc/apt/sources.list.d/vscodium.list
# sudo nala update && sudo nala install -y codium

# install vscode
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg
sudo nala update && sudo nala install -y code # or code-insiders

# LunaVim
# Node
# installs NVM (Node Version Manager)
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
# download and install Node.js
nvm install 20
# verifies the right Node.js version is in the environment
node -v # should print `v20.12.2`
# verifies the right NPM version is in the environment
npm -v # should print `10.5.0`
# Rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
# ripgrep
curl -LO https://github.com/BurntSushi/ripgrep/releases/download/14.1.0/ripgrep_13.0.0_amd64.deb
sudo dpkg -i ripgrep_14.1.0_amd64.deb
# finall: Lvim
LV_BRANCH='release-1.3/neovim-0.9' bash <(curl -s https://raw.githubusercontent.com/LunarVim/LunarVim/release-1.3/neovim-0.9/utils/installer/install.sh)


# Lazygit
LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*')
curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"
tar xf lazygit.tar.gz lazygit
sudo install lazygit /usr/local/bin

# NerdFont
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.3.3/RobotoMono.zip
unzip RobotoMono.zip 
sudo mv R* /usr/share/fonts 
rm LICENSE.txt readme.md 


# ===============================
# ==========AI & LLMs ===========
# ===============================
# ollama
curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey \
    | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg
curl -s -L https://nvidia.github.io/libnvidia-container/stable/deb/nvidia-container-toolkit.list \
    | sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' \
    | sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list
sudo nala update

sudo nala install -y nvidia-container-toolkit

sudo nvidia-ctk runtime configure --runtime=docker
sudo systemctl restart docker

docker run -d --gpus=all -v ollama:/root/.ollama -p 11434:11434 --name ollama ollama/ollama

# open-webui
docker run -d -p 3000:8080 --add-host=host.docker.internal:host-gateway -v open-webui:/app/backend/data --name open-webui --restart always ghcr.io/open-webui/open-webui:main

# nvtop
sudo add-apt-repository ppa:flexiondotorg/nvtop
sudo nala install nvtop -y