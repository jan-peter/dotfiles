" JP: Welcome to my vim experience.


" call plug#begin('~/.vim/plugged')
call plug#begin('~/.config/nvim/plugged')
      Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
      Plug 'p00f/nvim-ts-rainbow'
      Plug 'voldikss/vim-floaterm'
      " Plug 'kassio/neoterm'
      
      Plug 'vim-test/vim-test'
      Plug 'kdheepak/lazygit.nvim'
      Plug 'sbdchd/neoformat'
      Plug 'tpope/vim-commentary'
      Plug 'preservim/tagbar'

      Plug 'folke/tokyonight.nvim'
      " Plug 'drewtempelmeyer/palenight.vim'
      
      Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}
      " 9000+ Snippets
      Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}
      " tab bar
      Plug 'kyazdani42/nvim-web-devicons'
      Plug 'romgrk/barbar.nvim'
call plug#end()

" set background=light "light, dark
" set termguicolors
" let g:tokyonight_style = "storm"
" colorscheme tokyonight
" colorscheme palenight
set t_Co=256
" hi! Normal ctermbg=NONE guibg=NONE
" hi! NonText ctermbg=NONE guibg=NONE guifg=NONE ctermfg=NONE

set mouse=a
set splitright
set splitbelow
set number "relativenumber

set expandtab
set shiftwidth=4
set tabstop=4 
set softtabstop=4
set autoindent

set hidden
set scrolloff=8
set sidescrolloff=8 

set incsearch
set clipboard=unnamedplus   " using system clipboard
set ttyfast                 " Speed up scrolling in Vim

set noswapfile encoding=UTF-8 nobackup nowritebackup
set fileformat=unix

filetype plugin indent on 

let mapleader="^"

" move a line up or down
" nmap n :m +1<CR>
" nmap m :m -2<CR>

" toogle lExplore
inoremap <leader>b <Esc>:Lex<cr>
nnoremap <leader>b <Esc>:Lex<cr>

" Toggle Folding
nnoremap <leader><C-l> :set nofoldenable<CR>
nnoremap <C-l> :set foldmethod=indent<CR>
nnoremap <leader>z zi

" tagbar
nmap <F8> :TagbarToggle<CR>


" float-term
let g:floaterm_keymap_toggle = '<leader>ft'
nnoremap   <silent>   <leader>f   :FloatermToggle<CR>
tnoremap   <silent>   <leader>f   <C-\><C-n>:FloatermToggle<CR>


" select python version
let g:syntastic_python_python_exec = 'python3' 

" vim-test
let test#python#runner='pytest'
let test#strategy='neovim'
" let test#strategy='floaterm'
nmap <silent> <leader>t :TestNearest<CR>
nmap <silent> <leader>T :TestFile<CR>
nmap <silent> <leader>a :TestSuite<CR>
nmap <silent> <leader>l :TestLast<CR>
nmap <silent> <leader>g :TestVisit<CR>

" setup mapping to call :LazyGit
nnoremap <silent> <leader>gg :LazyGit<CR>

" gcc for (un)commenting

"rainbow-parent
lua << EOF
require("nvim-treesitter.configs").setup {
  highlight = {
      -- ...
  },
  -- ...
  rainbow = {
    enable = true,
    -- disable = { "jsx", "cpp" }, list of languages you want to disable the plugin for
    extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
    max_file_lines = nil, -- Do not enable for files with more than n lines, int
    -- colors = {}, -- table of hex strings
    -- termcolors = {} -- table of colour name strings
  }
}
EOF

" coq.vim
let g:coq_settings = { 'auto_start': 'shut-up' }

" auto-close brackets
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O
