# Install my nvim setup

wget https://gitlab.com/jan-peter/dotfiles/-/raw/main/nvim/apt_installscript.sh \
chmod +x apt_installscript.sh\
./apt_installscript.sh


# Update init.vim
mv ~/.config/nvim/init.vim ~/.config/nvim/init_old.vim && wget -O ~/.config/nvim/init.vim https://gitlab.com/jan-peter/dotfiles/-/raw/main/nvim/init.vim
