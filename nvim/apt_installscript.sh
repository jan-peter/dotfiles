#!/bin/bash

apt update
apt install neovim wget tar

wget https://github.com/jesseduffield/lazygit/releases/download/v0.35/lazygit_0.35_Linux_x86_64.tar.gz
tar -xvzf lazygit_0.35_Linux_x86_64.tar.gz
chmod +x lazygit
sudo cp lazygit /usr/local/bin
sudo rm LICENSE README.md lazygit_0.35_Linux_x86_64.tar.gz

mkdir -p ~/.config/nvim
cp ~/.config/nvim/init.vim ~/.config/nvim/init_old.vim
rm ~/.config/nvim/init.vim
wget https://gitlab.com/jan-peter/dotfiles/-/raw/main/nvim/init.vim
mv ./init.vim ~/.config/nvim/init.vim

sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

nvim ~/.config/nvim/init.vim +PlugInstall
