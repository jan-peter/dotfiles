# DevelopmentStyle
- Use Test-driven Development (TDD) to always ensure the feature we try to build is implemented as intended
- don't forget to define the tests in a flexible way. Test which are too strict, might fail because they test too specific conditions
- always prefer British English over American English
- always state a plan first before you dive into generating code and working on the solution
- Use Agile Development by describing User Stories, tasks and acceptance criteria in backlog.md file
- please give every user story a numner for easier reference. But the numbering MUST NEVER CHANGE.
So, we just count the numbers in the user story upwards and if we prioritise them for their importance or impact, its totally ok to have them not in numeric order.
- Also, add tasks to each the user stories (also with numbers) to break the userstory in to small tasks and reference them easier
- Add acceptance criterias
- Add checkboxes to user stories, tasks and acceptance criteria to easily check the progress
- sort the user stories and tasks for priority
- if a user story is completed, put the the whole story in a seperate section at the end of the backlog
- Can you add a progress bar to the titles of the user stories? It should always represent the amout of tasks done per user story. I like this style: `- [ ] US2: Project Overview Dashboard [▰▰▰▱▱▱▱▱▱▱▱▱] 3/12 tasks`
- prefer simple and expressive code or even complete solutions
- NEVER automatically add or event commit changes to git. Always wait for my command on doing that.
- NEVER NEVER NEVER EVER add commit or even push to git without my command
- please dont forget to always try to minise complexity. The easier solution is oftern prefered

# Strategy for fixing failing tests
<!-- 
Lets run the full npm tests in verbose and reporter mode (base command: npx vitest run --reporter=verbose) and take a look at the outcome

Then, pick the first failing test and follow the strategy for fixing failing tests as layed out in @dev-rules.mdc 
Don't forget to add many many debug statements
dont forget to design the test in a flexible way. Dont be too specific
It is important to let the tests run all the way through and not cancle it. It may take a couple of seconds to run the tests. That is alright.
-->
1. Initial Investigation
- Run specific test file to get detailed error output
- Add debug statements to understand the current state
- Identify exact failure points and error messages
2. Root Cause Analysis
- Compare test expectations with actual implementation
- Check both the test file and the implementation file
- Look for mismatches between how tests expect things to work vs. how they actually work
3. Two-Sided Fix Approach
- First, update the implementation to handle all use cases (e.g., Project class's addTask method to handle both string and object inputs)
- Then, update the tests to match the actual behavior (e.g., updating assertions to check individual properties instead of using toContain)
- This ensures both the code and tests are aligned and maintainable
4. Verification
- Run tests again to verify fixes
- If new issues appear, iterate through the process
- Keep debug statements until all tests pass
5. Progressive Enhancement
- Once basic functionality works, add support for edge cases
- Maintain backward compatibility while adding new features
- Ensure all test cases still pass after each enhancement

**Generally**: Don't forget to design the test in a flexible way. Please don't be too specific.


