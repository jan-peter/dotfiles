#! /bin/sh

sudo apt update

# Remove Snap
snap list
sudo snap remove *
sudo apt purge snapd
sudo apt-mark hold snapd
apt list --installed | grep snap

# Install nala for faster and more beautiful apt
sudo apt install nala -y

# Set up nala
sudo nala fetch --auto
sudo nala update
sudo nala upgrade -y

# Install packages
sudo nala install gcc make perl curl wget htop btop git nvim neofetch 

# Install openbox and dependencies
sudo nala install openbox obconf xterm x11-xserver-utils